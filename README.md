# KoopaFundMe

### Descrição:

Site de crowdfunding para financiar a ida do Mario e do Luigi para o torneio de Super Smash Bros Ultimate. Pessoas podem se cadastrar no site para doar qualquer quantidade de dinheiro pelos mais diversos meios de pagamento para ajudar o Mario e o Luigi. Quanto maior a doação, maior o prêmio recebido. Projeto final do processo de treinamento 2018/2 da Struct, empresa júnior do curso de Engenharia da computação da Universidade de Brasília. 

### Desenvolvedores:

| Nome | Matrícula |
| --- | --- |
| André Filipe Caldas Laranjeira | 16/0023777 |
| Hugo Nascimento Fonseca | 16/0008166 |

##### Rails configuration:

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
