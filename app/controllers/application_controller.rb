class ApplicationController < ActionController::Base
  before_action :require_login, except: [:index]
  helper_method :store_errors_in_flash

  def index
    @users_count = User.count
    @distinct_countries = User.distinct.count(:country)
  end

  def not_authenticated
    flash.clear
    flash[:danger] = [["What are you doing?", "You must be authenticated to access this resource"]]
    redirect_to log_in_path
  end

  def store_errors_in_flash(target)
    if target.errors.any?
      target.errors.full_messages.each do |msg|
        if flash[:danger].nil?
          flash[:danger] = [["Oops!", msg + '!']]
        else
          flash[:danger] << [["Oops!", msg + '!']]
        end
      end
    end
  end

end
