class CampaignsController < ApplicationController
  before_action :require_login, except: [:index, :find_by_user, :show]

  def index
    @campaigns = Campaign.all.page(params[:page])
  end

  def edit
    @campaign = Campaign.find_by(id: params[:id])

    if @campaign.user_id != current_user.id
      flash[:danger] = [["Oh, no!", "You cannot edit this campaign!"]]
      redirect_to campaign_details_path(@campaign.id)
    end
  end

  def update
    @campaign = Campaign.find_by(id: params[:id])

    if @campaign.user_id != current_user.id
      flash[:danger] = [["Oh, no!", "You cannot edit this campaign!"]]
      redirect_to campaign_details_path(@campaign.id)
    end

    unless params[:cover_image].nil?
      @campaign.cover_image.destroy
      @campaign.cover_image.attach(params[:cover_image])
    end

    if @campaign.update_attributes(campaign_edit_params)
      flash[:success] = [["Alright!", "Your campaign was updated"]]
      redirect_to campaign_details_path(@campaign.id)
    else
      render 'edit'
    end
  end

  def delete_cover_image
    @campaign = Campaign.find_by(id: params[:id], user_id: current_user.id)
    @campaign.cover_image.destroy
    flash[:success] = [['All good!', 'Campaign cover image has been deleted']]
    redirect_to edit_campaign_path
  end

  def create
    @campaign = Campaign.new(campaign_creation_params)
    @campaign.current_amount = 0          # All campaigns start at 0 dollars.
    @campaign.user_id = current_user.id   # The current user is the creator.

    if @campaign.save
      flash.clear
      flash[:success] = [["", "Campaign successfully created!"]]
      redirect_to campaigns_path
    else
      render 'new'
    end
  end

  def find_by_user
    @user = User.find(params[:id])
    @campaigns = Campaign.where(:user_id => params[:id]).page(params[:page])
  end

  def new
    @campaign = Campaign.new
  end

  def show
    @campaign = Campaign.find(params[:id])
    @creator = User.find(@campaign.user_id) unless @campaign.user_id.nil?
    unless current_user.nil?
      @contribution = Contribution.where(campaign_id: @campaign.id,
                                         user_id: current_user.id).first
      @user_rewards = current_user.rewards
    end
    @contribution = Contribution.new if @contribution.nil?
    @user_rewards = [] if @user_rewards.nil?
  end

  private
    def campaign_creation_params
      params.require(:campaign).permit(:title, :description, :desired_amount)
    end

    def campaign_edit_params
      params.require(:campaign).permit(:title, :description, :cover_image)
    end

end
