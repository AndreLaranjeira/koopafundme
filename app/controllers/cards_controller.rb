class CardsController < ApplicationController
  before_action :require_login

  def manage
    @card = Card.new
    @cards = Card.where(user_id: current_user.id)
  end

  def show
    @cards = Card.where(user_id: current_user.id)
  end

  def create
    @card = Card.new(card_params)
    @card.user = current_user
    begin
      if @card.save
        flash[:success] = [["Completed!", "Payment methods updated"]]
      else
        store_errors_in_flash(@card)
      end
      redirect_to manage_payment_path
    end
  end

  def update
    @card = Card.find(params[:id])
    begin
      if @card.update_attributes(card_params)
        flash[:success] = [['Completed!', 'Payment methods updated']]
      else
        store_errors_in_flash(@card)
      end
      redirect_to manage_payment_path
    end
  end

  def destroy
    @card = Card.find(params[:id])
    @card.destroy
    flash[:success] = [['Done!', 'Your card has been deleted']]
    redirect_to manage_payment_path
  end

  private

  def card_params
    params.require(:card).permit(:card_number, :card_cvv,
                                 :card_valid_until, :card_owners_name)
  end

end
