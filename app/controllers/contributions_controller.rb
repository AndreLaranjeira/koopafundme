class ContributionsController < ApplicationController
  before_action :require_login

  def index
    @contributions = Contribution.where(user_id: current_user.id).order(created_at: :desc).page(params[:page])
  end

  def create
    @contribution = Contribution.new(contribution_params)
    @contribution.user_id = current_user.id
    @campaign = Campaign.find(@contribution.campaign_id)
    @user = current_user

    if @user.wallet_amount >= @contribution.value

      if @contribution.save

        # Transfer the funds:
        contribution_value = @contribution.value
        @user.wallet_amount -= contribution_value
        @campaign.current_amount += contribution_value

        # Add the rewards:
        @campaign.rewards.each do |reward|
          @user.rewards << reward unless (contribution_value < reward.min_value)
        end

        # Save changes:
        @user.save
        @campaign.save

        flash[:success] = [["Success!", "Thank you for contributing!"]]

      else
        flash[:danger] = [["Oh, no!",
                           "A problem occurred with your contribution!"]]
      end

    else
      flash[:danger] = [["Oh, no!",
                         "Not enough funds to make that contribution!"]]
    end

    redirect_to campaign_details_path(@campaign.id)

  end

  private
    def contribution_params
      params.require(:contribution).permit(:value, :campaign_id, :user_id)
    end

end
