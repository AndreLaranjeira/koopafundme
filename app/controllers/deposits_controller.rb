class DepositsController < ApplicationController
  before_action :require_login

  def index
    @deposits = Deposit.where(user_id: current_user.id).page(params[:page])
  end

end
