class PhonesController < ApplicationController
  before_action :require_login

  def create
    @phone = Phone.new(phone_params)
    @phone.user = current_user

    if @phone.save
      flash.clear
      flash[:success] = [['Done!', 'Phone number added to your profile']]
      redirect_to profile_path(current_user.id)
    else
      flash[:danger] = [['Something is wrong!', 'Phone number could not be added']]
      render 'users/show'
    end
  end

  def destroy
    @phone = Phone.find(params[:id])
    @phone.destroy
    flash.clear
    flash[:info] = [['Thrown away!', 'Phone number removed from your profile']]
    redirect_to profile_path(current_user.id)
  end

  private

  def phone_params
    params.permit(:phone_type, :number)
  end
end
