class RewardsController < ApplicationController
  before_action :require_login

  def index
    @rewards = User.find_by(id: current_user.id).rewards.page(params[:page])
  end

end
