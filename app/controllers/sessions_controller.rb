class SessionsController < ApplicationController
  skip_before_action :require_login, except: [:destroy]
  skip_before_action :verify_authenticity_token, only: [:destroy]

  def new
    if current_user
      redirect_to users_path(current_user)
    end
  end

  def create
    if login(params[:email], params[:password], params[:remember])
      flash.clear
      flash[:success] = [["Welcome back, " + current_user.first_name + "!", "It's good to see you again"]]
      redirect_back_or_to home_path
    else
      flash.clear
      flash[:danger] = [["Oops!", "Invalid login credentials"]]
      render 'new'
    end
  end

  def destroy
    logout
    flash[:success] = [["See you!", "You've successfully logged out"]]
    redirect_to log_in_path
  end
end
