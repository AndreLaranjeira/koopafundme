class UsersController < ApplicationController
  before_action :require_login, except: [:index, :show, :new, :create]

  def index
    @users = User.all.page(params[:page])
  end

  def home
    @users = User.order(created_at: :desc).page(params[:users_page]).per(5)
    @campaigns = Campaign.order(created_at: :desc).page(params[:campaigns_page]).per(5)
  end

  def show
    @user = User.find(params[:id])
  end

  def update
    @user = current_user
    unless params[:profile_image].nil?
      @user.profile_image.destroy
      @user.profile_image.attach(params[:profile_image])
    end
    if @user.update_attributes(user_edit_params)
      flash[:success] = [["Alright!", "Your profile was updated"]]
      redirect_to profile_path(@user.id)
    else
      render 'edit'
    end
  end

  def edit
    @user = current_user
  end

  def create
    @user = User.new(user_create_params)
    @user.user_role_id = 2  # All users created don't have administrator
                            # privileges. They are normal users.
    @user.wallet_amount = 0.0 # All users start with empty wallet
    if @user.save
      flash.clear
      flash[:success] = [["", "User account successfully created!"]]
      redirect_to log_in_path
    else
      render 'new'
    end
  end

  def new
    @user = User.new
  end

  def destroy
    @user = current_user
    @user.destroy
    flash.clear
    flash[:info] = [["Goodbye!", "Your account has been deleted!"]]
    redirect_to root_path
  end

  def delete_profile_image
    @user = current_user
    @user.profile_image.destroy
    flash[:success] = [['All good!', 'Your profile image has been deleted']]
    redirect_to edit_profile_path
  end

  private
    def user_create_params
      params.require(:user).permit(:first_name, :last_name, :email, :password,
                                   :password_confirmation, :birth_date)
    end

    def user_edit_params
      params.require(:user).permit(:first_name, :last_name, :email, :birth_date,
                                   :address, :city, :zip_code, :country, :profile_image)
    end

end
