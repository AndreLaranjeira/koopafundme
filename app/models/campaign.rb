class Campaign < ApplicationRecord
  # Cover image via ActiveStorage
  has_one_attached :cover_image

  belongs_to :user, optional: true
  has_many :contributions, dependent: :delete_all
  has_many :rewards, dependent: :delete_all

  def count_contributors
    self.contributions.size
  end

  def funding_percentage
    (100 * self.current_amount/self.desired_amount).round(2)
  end

end
