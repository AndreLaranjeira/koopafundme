class Card < ApplicationRecord
  belongs_to :user
  has_many :deposits, dependent: :nullify

  validates :card_number, length: {is: 16}
  validates_uniqueness_of :card_number, scope: :user_id,
                          message: "is already registered to your name"

  def get_card_network
    networks = [{pattern: /^4[0-9]{12}(?:[0-9]{3})/, details: {name: "Visa", html: '<i class="fab fa-cc-visa"></i>'}},
                {pattern: /^5[1-5][0-9]{14}/, details: {name: "Mastercard", html: '<i class="fab fa-cc-mastercard"></i>'}},
                {pattern: /^3[47][0-9]{13}/,  details: {name: "Amex", html: '<i class="fab fa-cc-amex"></i>'}},
                {pattern: /^3(?:0[0-5]|[68][0-9])[0-9]{11}/, details: {name: "Diners Club", html: '<i class="fab fa-cc-diners-club"></i>'}},
                {pattern: /^6(?:011|5[0-9]{2})[0-9]{12}/, details: {name: "Discover", html: '<i class="fab fa-cc-discover"></i>'}},
                {pattern: /^(?:2131|1800|35\d{3})\d{11}/, details: {name: "JCB", html: '<i class="fab fa-cc-jcb"></i>'}},
                {pattern: /^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})$/, details: {name: "Elo", html: '<i class="fas fa-credit-card"></i>'}},
                {pattern: /^(606282\d{10}(\d{3})?)|(3841\d{15})$/, details: {name: "Hipercard", html: '<i class="fas fa-credit-card"></i>'}}]
    networks.each do |network|
        if network[:pattern].match?(self.card_number)
          return network[:details]
        end
    end
    {name: 'Unknown', html: '<i class="fas fa-credit-card"></i>'}
  end
end
