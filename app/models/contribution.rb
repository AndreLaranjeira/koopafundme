class Contribution < ApplicationRecord
  validates :campaign_id, presence: true

  belongs_to :user, optional: true
  belongs_to :campaign
end
