class Deposit < ApplicationRecord
  belongs_to :card, optional: true
  belongs_to :payment_method
  belongs_to :transaction_state
  belongs_to :user
end
