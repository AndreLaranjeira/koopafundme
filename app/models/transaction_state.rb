class TransactionState < ApplicationRecord
  has_many :contributions
  has_many :deposits
end
