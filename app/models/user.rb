class User < ApplicationRecord
  # Profile image via ActiveStorage
  has_one_attached :profile_image

  # Sorcery gem:
  authenticates_with_sorcery!
  attr_accessor :skip_password

  # Validation:
  validates :password, length: {minimum: 8},
            if: -> {new_record? || changes[:crypted_password]},
            unless: :skip_password
  validates :password, confirmation: true,
            if: -> {new_record? || changes[:crypted_password]}
  validates :password_confirmation, presence: true,
            if: -> {new_record? || changes[:crypted_password]}
  validates :email, uniqueness: true


  # Relationships:
  belongs_to :user_role
  has_and_belongs_to_many :rewards, dependent: :delete_all
  has_many :cards, dependent: :delete_all
  has_many :campaigns
  has_many :contributions
  has_many :deposits, dependent: :delete_all
  has_many :phones, dependent: :delete_all

  # Methods:
  def amount_contributed
    amount = 0.00
    self.contributions.each do |donation|
      amount += donation.value
    end
    amount
  end

  def amount_received
    amount = 0.00
    self.campaigns.each do |campaign|
      amount += campaign.current_amount
    end
    amount
  end

  def count_contributions
    self.contributions.size
  end

  def count_campaigns
    self.campaigns.size
  end

  def join_date
    day = self.created_at.day.to_s
    mount = self.created_at.month.to_s
    year = self.created_at.year.to_s
    "#{day}/#{mount}/#{year}"
  end

  def update_date
    day = self.updated_at.day.to_s
    mount = self.updated_at.month.to_s
    year = self.updated_at.year.to_s
    "#{day}/#{mount}/#{year}"
  end

  def format_birth_date
    day = self.birth_date.day.to_s
    mount = self.birth_date.month.to_s
    year = self.birth_date.year.to_s
    "#{day}/#{mount}/#{year}"
  end

end
