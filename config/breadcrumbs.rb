crumb :root do
  link 'Home', (current_user.nil? ? root_path : home_path)
end

crumb :user_index do
  link 'All users', users_path
  parent :root
end

crumb :contributions do
  link 'Contributions', contributions_path
  parent :user, current_user
end

crumb :financial do
  link 'Financial', deposits_path
  parent :root
end

crumb :rewards do
  link 'Owned rewards', rewards_path
  parent :user, current_user
end

crumb :user do |user|
  link user.first_name + '\'s profile', profile_path(user.id)
  parent :user_index
end

crumb :payment do
  link 'Payment methods', payment_methods_path
  parent :user, current_user
end

crumb :payment_manager do
  link 'Managing', manage_payment_path
  parent :payment
end

crumb :user_edit do |user|
  link 'Editing', edit_profile_path
  parent :user, user
end

crumb :user_campaigns do |user|
  link 'Campaigns of ' + user.first_name, user_campaigns_path(user.id)
  parent :campaigns_index
end

crumb :campaigns_index do
  link 'All campaigns', campaigns_path
  parent :root
end

crumb :campaign_new do
  link 'Starting your campaign', new_campaign_path
  parent :root
end

crumb :campaign do |campaign|
  link campaign.title, campaign_path(campaign.id)
  parent :campaigns_index
end

crumb :campaign_edit do |campaign|
  link 'Editing', edit_campaign_path
  parent :campaign, campaign
end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).
