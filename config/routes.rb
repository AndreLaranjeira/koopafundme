Rails.application.routes.draw do
  root "application#index"

  resources :campaigns, only:[:index, :create, :update]
  get '/user/:id/campaigns', to: 'campaigns#find_by_user', as: :user_campaigns
  get '/new_campaign', to: 'campaigns#new', as: :new_campaign
  get '/campaign/:id', to: 'campaigns#show', as: :campaign_details
  get '/campaign/:id/edit', to: 'campaigns#edit', as: :edit_campaign
  delete '/campaign/:id/delete_cover_image', to: 'campaigns#delete_cover_image', as: :delete_cover_image

  resources :contributions, only:[:index, :create]

  resources :deposits, only:[:index]

  resources :rewards, only:[:index]

  resources :sessions, only:[:create]
  get '/log_in', to: 'sessions#new', as: :log_in
  delete '/log_out', to: 'sessions#destroy', as: :log_out

  resources :users, only:[:index, :create, :update]
  get '/home', to: 'users#home', as: :home
  get '/profile/:id', to: 'users#show', as: :profile
  delete '/delete_profile', to: 'users#destroy', as: :delete_profile
  get '/sign_up', to: 'users#new', as: :sign_up
  get '/edit_profile', to: 'users#edit', as: :edit_profile
  delete '/delete_profile_image', to: 'users#delete_profile_image', as: :delete_profile_image

  resources :phones, only:[:create]
  delete '/profile/phones/:id/delete', to: 'phones#destroy', as: :delete_phone

  resources :cards, only:[:create, :update]
  get 'payment_methods/cards', to: 'cards#show', as: :payment_methods
  get 'payment_methods/manage', to: 'cards#manage', as: :manage_payment
  delete 'payment_methods/delete_card/:id', to: 'cards#destroy', as: :delete_card
end
