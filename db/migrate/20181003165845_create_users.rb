class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :complete_name
      t.date :birth_date
      t.string :cpf
      t.string :country
      t.string :address
      t.string :city
      t.string :email
      t.string :password
      t.float :wallet_amount

      t.timestamps
    end
  end
end
