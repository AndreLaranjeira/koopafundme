class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.string :card_number
      t.string :card_cvv
      t.date :card_valid_until
      t.string :card_owners_name
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
