class CreateJoinTableUserPhones < ActiveRecord::Migration[5.2]
  def change
    create_join_table :phones, :users do |t|
      t.index [:user_id, :phone_id]
    end
  end
end
