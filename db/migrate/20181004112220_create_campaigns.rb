class CreateCampaigns < ActiveRecord::Migration[5.2]
  def change
    create_table :campaigns do |t|
      t.string :title
      t.text :description
      t.float :desired_amount
      t.float :current_amount
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
