class CreateJoinTableRewardsUsers < ActiveRecord::Migration[5.2]
  def change
    create_join_table :users, :rewards do |t|
      t.index [:user_id, :reward_id]
    end
  end
end
