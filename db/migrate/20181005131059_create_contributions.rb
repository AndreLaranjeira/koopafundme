class CreateContributions < ActiveRecord::Migration[5.2]
  def change
    create_table :contributions do |t|
      t.float :value
      t.references :user, foreign_key: true
      t.references :campaign, foreign_key: true
      t.references :transaction_state, foreign_key: true

      t.timestamps
    end
  end
end
