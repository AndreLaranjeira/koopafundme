class DropJoinTableUserPhones < ActiveRecord::Migration[5.2]
  def change
    drop_join_table :phones, :users do |t|
      t.index [:user_id, :phone_id]
    end
  end
end
