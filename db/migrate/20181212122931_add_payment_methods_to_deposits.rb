class AddPaymentMethodsToDeposits < ActiveRecord::Migration[5.2]
  def change
    add_reference :deposits, :payment_method, foreign_key: true
    remove_column :deposits, :method
    change_column_null :deposits, :card_id, true
  end
end
