class SorceryCore < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :salt, :string
    add_index :users, :email, unique: true
    change_column_null :users, :email, false
    rename_column :users, :password, :crypted_password
  end
end
