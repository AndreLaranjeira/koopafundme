class RemoveStateFkFromContribution < ActiveRecord::Migration[5.2]
  def change
    remove_column :contributions, :transaction_state_id
  end
end
