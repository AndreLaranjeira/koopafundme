# Control panel:

# Variable to control if the database is cleaned up before seeding.
# => false: No tables are cleaned up before seeding.
# WARNING: Do not use this option if the entries to be seeded have specify an
# id being used by the database!
# => true: All tables are cleaned up before seeding.

clear_tables = true

# Variable to control if seed examples are inserted in database.
# => false: Only control tables are seeded.
# => true: Control tables and data tables are seeded.

seed_data_tables = true

# Seeds:

campaigns = [{id: 1, title: "Super Smash Bros. Tournament",
              description: "Help me and Luigi make it to the Super Smash Bros. Tournament!",
              desired_amount: 5000, current_amount: 0, user_id: 3},
             {id: 2, title: "Bugstenium Software co.",
              description: "Me and a group of friends want to create a programming start-up. Wanna help?",
              desired_amount: 10000, current_amount: 4300, user_id: 1},
             {id: 3, title: "Star Wars VII remake",
              description: "As the creator of Star Wars, I want a chance to reedem myself by remaking the sequel trilogy.",
              desired_amount: 250000, current_amount: 3450, user_id: 5},
             {id: 4, title: "The Legend of Zelda: Breath of the Wild II",
              description: "A whole new adventure for Link, Zelda and friends.",
              desired_amount: 130000, current_amount: 145745, user_id: 2}]

cards = [{id: 1, card_number: 1234567812345678, card_cvv: 1234,
          card_valid_until: '31-Dec-9999', card_owners_name: 'Tester',
          user_id: 1},
         {id: 2, card_number: 8765432112345678, card_cvv: 2323,
          card_valid_until: '01-Aug-2020', card_owners_name: 'Shigeru Miyamoto',
          user_id: 2},
         {id: 3, card_number: 8765432109876543, card_cvv: 9876,
          card_valid_until: '01-Nov-2022', card_owners_name: 'Mario',
          user_id: 3}]

contributions = [{id: 1, value: 4000, user_id: 2, campaign_id: 2},
                 {id: 2, value: 300, user_id: 5, campaign_id: 2},
                 {id: 3, value: 3450, user_id: 2, campaign_id: 3},
                 {id: 4, value: 40000, user_id: 1, campaign_id: 4},
                 {id: 5, value: 100000, user_id: 5, campaign_id: 4},
                 {id: 6, value: 5745, user_id: 7, campaign_id: 4},]

deposits = [{id: 1, value: 500, transaction_state_id: 4, user_id: 3,
             payment_method_id: 2},
            {id: 2, value: 4000, transaction_state_id: 1, user_id: 2,
             payment_method_id: 1, card_id: 2},
            {id: 3, value: 500, transaction_state_id: 1, user_id: 1,
             payment_method_id: 2},
            {id: 4, value: 303450, transaction_state_id: 1, user_id: 2,
             payment_method_id: 2},
            {id: 5, value: 800000, transaction_state_id: 1, user_id: 2,
             payment_method_id: 2},
            {id: 6, value: 10, transaction_state_id: 1, user_id: 3,
             payment_method_id: 2},
            {id: 7, value: 2, transaction_state_id: 1, user_id: 4,
             payment_method_id: 2},
            {id: 8, value: 100300, transaction_state_id: 1, user_id: 5,
             payment_method_id: 2},
            {id: 9, value: 460998007, transaction_state_id: 1, user_id: 5,
             payment_method_id: 2},
            {id: 10, value: 60, transaction_state_id: 1, user_id: 6,
             payment_method_id: 2},
            {id: 11, value: 6245, transaction_state_id: 1, user_id: 7,
             payment_method_id: 2}]

payment_methods = [{id: 1, method: 'Credit card'},
                   {id: 2, method: 'PayPal'}]

phones = [{id: 1, phone_type:  'Fixo', number: '556133659987', user_id: 1},
          {id: 2, phone_type:  'Celular', number:  '5561991256363', user_id: 1},
          {id: 3, phone_type:  'Celular', number:  '5561991256364', user_id: 2},
          {id: 4, phone_type:  'Fixo', number: '556133752014', user_id: 2},
          {id: 5, phone_type:  'Celular', number: '5598855214475', user_id: 3},
          {id: 6, phone_type:  'Fixo', number: '556154637128', user_id: 5},
          {id: 7, phone_type:  'Fixo', number: '55611113324', user_id: 6},
          {id: 8, phone_type:  'Celular', number: '556112344321', user_id: 7}]

rewards = [{id: 1, name: "Mario plushie", min_value: "100", campaign_id: 1},
           {id: 2, name: "Nintendo Switch", min_value: "700", campaign_id: 1},
           {id: 3, name: "Bugstenium shirt", min_value: "100", campaign_id: 2},
           {id: 4, name: "Bugstenium Test Suite", min_value: "1000",
            campaign_id: 2},
           {id: 5, name: "Coaxium cell", min_value: "1000", campaign_id: 3},
           {id: 6, name: "Lightsaber", min_value: "50000", campaign_id: 3},
           {id: 7, name: "Real Link sword", min_value: "2000", campaign_id: 4},
           {id: 8, name: "Dinner with Miyamoto", min_value: "100000",
            campaign_id: 4}]

transaction_states = [{id: 1, name: 'Approved'},
                      {id: 2, name: 'Waiting payment'},
                      {id: 3, name: 'Canceled'},
                      {id: 4, name: 'Reimbursed'}]

users = [{id: 1, first_name: 'John', last_name: 'Tester',
          birth_date: '26-Oct-1958', country: 'Brazil',
          address: 'University of Brasilia', city: 'Brasilia',
          email: 'test@test.com', password: 'password',
          password_confirmation: 'password',  zip_code: '70000-000',
          wallet_amount: 1000000, user_role_id: 1},
         {id: 2, first_name: 'Shigeru', last_name: 'Miyamoto',
          birth_date: '16-Nov-1952', country: 'Japan',
          address: 'Nintendo studios', city: 'Tokyo',
          email: 'Miyamoto_Shigeru@nintendo.com',
          password: 'ICreatedSuperMario',
          password_confirmation: 'ICreatedSuperMario', wallet_amount: 1000000,
          user_role_id: 1},
         {id: 3, first_name: 'Mario', last_name: 'Mushroom',
          birth_date: '9-Jul-1981', country: 'Japan',
          address: 'Mushroom Kingdom', city: 'Toad Town',
          email: 'RedPlumber@nintendo.com', password: 'ItsAMeMario',
          password_confirmation: 'ItsAMeMario', wallet_amount: 10,
          user_role_id: 2},
         {id: 4, first_name: 'Luigi', last_name: 'Mushroom',
          birth_date: '14-Jul-1983', country: 'Japan',
          address: 'Mushroom Kingdom', city: 'Toad Town',
          email: 'GreenPlumber@nintendo.com', password: 'ItsAMeLuigi',
          password_confirmation: 'ItsAMeLuigi', wallet_amount: 2,
          user_role_id: 2},
         {id: 5, first_name: 'George', last_name: 'Lucas',
          birth_date: '14-May-1944', email: 'DarthLucas@lucasfilms.com',
          password: 'IAmYourFather', password_confirmation: 'IAmYourFather',
          wallet_amount: 460998007, user_role_id: 2},
         {id: 6, first_name: 'Robert', last_name: 'Paar',
          birth_date: '26-Nov-1934', email: 'MrIncredible@insuricare.com',
          password: 'KronosKronos', password_confirmation: 'KronosKronos',
          wallet_amount: 60, user_role_id: 2},
         {id: 7, first_name: 'Jordan', last_name: 'Trace',
          birth_date: '14-Mar-1982', email: 'Thermite@Rainbow6.com',
          password: 'SpecialCharge', password_confirmation: 'SpecialCharge',
          wallet_amount: 500, user_role_id: 2}]

user_rewards = [{user_id: 2, reward_id: 3},
                {user_id: 2, reward_id: 4},
                {user_id: 5, reward_id: 3},
                {user_id: 2, reward_id: 5},
                {user_id: 1, reward_id: 7},
                {user_id: 5, reward_id: 7},
                {user_id: 5, reward_id: 8},
                {user_id: 7, reward_id: 7}]

user_roles = [{id: 1, role: 'Administrator'},
              {id: 2, role: 'Normal'}]

# Cleaning up the database:

if(clear_tables)

  # Declare all models in insertion order.

  model_names = [PaymentMethod, TransactionState, UserRole, User, Campaign, Card,
                 Phone, Contribution, Deposit, Reward]

  # Reverse the order to avoid FK constraints and delete all model entries.

  model_names.reverse.each do |model_name|
    model_name.delete_all
  end

end

# Populating process:

# Control tables should be populated first.

payment_methods.each do |payment_method|
  PaymentMethod.where(payment_method).first_or_create!
end

transaction_states.each do |transaction_state|
  TransactionState.where(transaction_state).first_or_create!
end

user_roles.each do |user_role|
  UserRole.where(user_role).first_or_create!
end

# Data tables are populated next.

if(seed_data_tables)

  # Users are populated next because they are referenced by many other tables.

  users.each do |user|
    User.create!(user)
  end

  # We them populate all tables that reference only the user table.

  campaigns.each do |campaign|
    Campaign.create!(campaign)
  end

  cards.each do |card|
    Card.create!(card)
  end

  phones.each do |phone|
    Phone.create!(phone)
  end

  # The remaining model tables are populated.

  contributions.each do |contribution|
    Contribution.create!(contribution)
  end

  deposits.each do |deposit|
    Deposit.create!(deposit)
  end

  rewards.each do |reward|
    Reward.create!(reward)
  end

  # The M to N relationships are populated last.

  user_rewards.each do |pair|
    User.find(pair[:user_id]).rewards << Reward.find(pair[:reward_id])
  end

end
