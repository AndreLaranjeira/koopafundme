#language: en
#encoding: utf-8

Feature: Create a crowdfunding campaign
  As a registered user
  So that I can obtain crowdfunding support from other users
  I want to create a crowdfunding campaign

Background:
  Given I loaded the database with the user roles control table

Scenario: Valid campaign creation
  Given I log in with a valid user account
  And I click the 'New campaign' button
  When I fill the form with the following information:
  |Campaign name           |Test campaign                   |
  |Campaign description    |This is only for a simple test! |
  |Desired campaign amount |5                               |
  And I submit the campaign form
  Then I should have created a new crowdfunding campaign
