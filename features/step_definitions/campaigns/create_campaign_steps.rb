# This is the steps file referred to create_campaign feature.
# Place your code relative to that feature here.

Given("I click the 'New campaign' button") do
  visit root_path
  click_on('New campaign')
end

Then("I should have created a new crowdfunding campaign") do
  expect(page).to have_current_path(root_path)
  expect(page).to have_content "Campaign successfully created!"
end
