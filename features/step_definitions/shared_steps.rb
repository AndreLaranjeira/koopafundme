# This is the steps file referred to all features.
# Place your common code to all features here.

Given("I loaded the database with the user roles control table") do
  UserRole.create!(id: 1, role: 'Administrator')
  UserRole.create!(id: 2, role: 'Normal')
end

Given("I log in with a valid user account") {
  steps %Q{
    Given I created an user with the email 'valid@test.com' and password 'testpassword'
    Given I click on the 'Log In' button
    When I fill the form with the following information:
    |Email                 |valid@test.com        |
    |Password              |testpassword          |
    And I submit the login form
  }
}

When("I fill the form with the following information:") do |table|
  table.rows_hash.each do |field, value|
    fill_in field, with: value
  end
end

When(/^I submit the .* form$/) do
  find('input[name="commit"]').click
end

Then("I should see the message {string}") do |msg|
  expect(page).to have_content msg
end
