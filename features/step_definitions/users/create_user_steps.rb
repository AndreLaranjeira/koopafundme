# This is the steps file referred to create_user feature.
# Place your code relative to that feature here.

Given("I click on the 'Sign up' button") do
  visit root_path
  click_on('Sign up')
end

Given ("I created an user with the email {string} and password {string}") do
  |email, password|
  steps %Q{
    Given I click on the 'Sign up' button
    When I fill the form with the following information:
    |Name                  |Account Tester        |
    |Email                 |#{email}              |
    |Password              |#{password}           |
    |Password confirmation |#{password}           |
    And I select the date of birth as '1991/January/1'
    And I submit the user creation form
    Then I should have successfully created an user account
  }
end

When("I select the date of birth as {string}") do |date_of_birth|
  data = date_of_birth.split('/')
  data.each_with_index do |element, index|
    select element, from: 'user_birth_date_' + (index + 1).to_s + 'i'
  end
end

When("I try to create an user with the email {string} and password {string}") do
  |email, password|
  steps %Q{
    Given I click on the 'Sign up' button
    When I fill the form with the following information:
    |Name                  |Account Tester        |
    |Email                 |#{email}              |
    |Password              |#{password}           |
    |Password confirmation |#{password}           |
    And I select the date of birth as '1991/January/1'
    And I submit the user creation form
  }
end

Then("I should have successfully created an user account") do
  expect(page).to have_current_path(root_path)
  expect(page).to have_content "User account successfully created!"
end
