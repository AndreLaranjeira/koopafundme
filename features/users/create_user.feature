#language: en
#encoding: utf-8

Feature: Create an user account
  As a person visiting the website
  So that I can create crowdfunding campaigns and contribute to them
  I want to create an user account in the website

Background:
  Given I loaded the database with the user roles control table

Scenario: Valid user account
  Given I click on the 'Sign up' button
  When I fill the form with the following information:
  |Name                  |Account Tester        |
  |Email                 |test_account@test.com |
  |Password              |testpassword          |
  |Password confirmation |testpassword          |
  And I select the date of birth as '1991/January/1'
  And I submit the user creation form
  Then I should have successfully created an user account

Scenario: Password too short
  When I try to create an user with the email 'test_account@test.com' and password 'test'
  Then I should see the message 'Password is too short (minimum is 8 characters)!'

Scenario: Wrong password confirmation
  Given I click on the 'Sign up' button
  When I fill the form with the following information:
  |Name                  |Account Tester        |
  |Email                 |test_account@test.com |
  |Password              |testpassword1         |
  |Password confirmation |testpassword2         |
  And I select the date of birth as '1991/January/1'
  And I submit the user creation form
  Then I should see the message "Password confirmation doesn't match Password!"

Scenario: Email already taken
  Given I created an user with the email 'test_account@test.com' and password 'testpassword'
  When I try to create an user with the email 'test_account@test.com' and password 'testpassword2'
  Then I should see the message 'Email has already been taken!'
